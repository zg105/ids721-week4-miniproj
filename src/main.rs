use actix_web::{get, App, HttpServer, Responder};
use rand::seq::SliceRandom;

#[get("/")]
async fn index() -> impl Responder {
    let messages = vec![
        "Welcome to Jason's website"
    ];

    // show the message
    let random_message = messages.choose(&mut rand::thread_rng()).unwrap_or(&"No messages available");

    format!("{}", random_message)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(index)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
