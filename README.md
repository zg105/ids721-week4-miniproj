# ids721-week4-miniProj



## Getting started
This project involves containerizing a simple Rust Actix web application using Docker. The goal is to create a self-contained environment for the web service, ensuring easy deployment and scalability.

## Setup

1. Create a Actix-web application
`cargo new week4-mini-project`

2. Write a simple website in main.rs, it will show a welcome message

3. Use Docker to containerize it

4. Build the Docker image
`sudo docker build -t week4-mini-project .`

5. Run the Docker image
`sudo docker run -p 8080:8080 week4-mini-project`
![](1.png)

![](2.png)
